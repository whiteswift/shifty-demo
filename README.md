# Shifty employee shift booking api
An API that allows a store’s employees to view and book shifts

# Install
`yarn`

# Develop
`yarn dev`

# Run

## Server
`yarn start`

Navigate to [http://localhost:3000/listing/](http://localhost:3000/listing/) to test the listing route

Send a POST request [http://localhost:3000/book/](http://localhost:3000/book/) with form data 

## Docker
`docker build --tag=shifty-app .`

`docker run --name shifty-container -p 8080:3000 shifty-app`

Navigate to [http://localhost:8080/listing/](http://localhost:8080/listing/) to test the listing route

Send a POST request [http://loca§lhost:8080/book/](http://localhost:8080/book/) with form data 

# Test
`yarn test`

# Documentation
Database ERD/Model

https://docs.google.com/drawings/d/1wriXg4JJTQhJu0VClXFySjWpmP60uMwaOOBFE9Vfn6o/edit?usp=sharing
