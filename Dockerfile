FROM node:10-slim
WORKDIR /app
COPY . /app
ENV NODE_ENV=production
RUN yarn
CMD [ "yarn", "start" ]