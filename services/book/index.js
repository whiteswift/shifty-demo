'use strict'
const concat = require('concat-stream')

module.exports = async (fastify, opts) => {
  fastify.register(require('fastify-formbody'))

  fastify.route({
    method: 'POST',
    url: '/book/:id',
    // schema: {
    //   // the response needs to be an object with an `hello` property of type 'string'
    //   response: {
    //     200: ''
    //   }
    // },
    // this function is executed for every request before the handler is executed
    preHandler: async (request, reply) => {
      // E.g. check authentication
    },
    handler: async (request, reply) => {
      // Do something with request.query

      try {
        // debugger
        await setBooking(request.params.id)
      }
      catch (err) {
        // TODO: Switch on code returned

        reply.code(500)
          .send()
        return;
      }

      reply.code(200)
        .send()
    }
  })

  async function setBooking(data) {
    // TODO: Set booking info with post data
    console.log('setBooking', data);
    return data;
  }
}
