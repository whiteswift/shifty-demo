'use strict'

module.exports = async (fastify, opts) => {
  fastify.get('/listing', (request, reply) => {
    reply.redirect('/listing/')
  })

  fastify.route({
    method: 'GET',
    url: '/listing/',
    // schema: {
    //     // the response needs to be an object with an `hello` property of type 'string'
    //     response: {
    //         200: {
    //             type: 'object',
    //             properties: {
    //                 listing: { type: 'object' }
    //             }
    //         }
    //     }
    // },
    // this function is executed for every request before the handler is executed
    preHandler: async (request, reply) => {
      // E.g. check authentication
    },
    handler: async (request, reply) => {
      // TODO: Get listings from DB
      // const listings = await getListings(request.query)
      // reply.code(200)
      // .header('Content-Type', 'application/json; charset=utf-8')
      // .send(listings)

      reply.code(200)
        .header('Content-Type', 'application/json; charset=utf-8')
        .send({
          listing: {
            london: {
              am: 'available', pm: 'unavailable'
            }
          }
        })
    }
  })

  async function getListings(data) {
    // TODO: Set booking info with post data
    console.log('getListings', data);
    return data;
  }
}
