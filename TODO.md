TODO

- Get a simple node app running
- Get routes working with mock data
	- /listing/
	- /book/:day
- Do some testing 
- Move routes to own folder
- Dockerfile and run as a container
- Database
    - connect in docker container
    - create tables 
- Do config
- Do gitlab continuous deploy pipeline
- Async await everything and load test

- Update README.md before submit